#!/bin/bash

set -euxo pipefail

DOCKER_IMAGE="openjdk:11.0.2"
CONTAINER_NAME="app"
PROGRAM_NAME="app-1.0.0.jar"

docker pull ${DOCKER_IMAGE}
docker run --rm -it --name ${CONTAINER_NAME} -v `pwd`:/root ${DOCKER_IMAGE} /bin/bash -c "\
cd root \
&& ./run.sh"