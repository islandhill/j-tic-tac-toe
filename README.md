# Tic Tac Toe Game API Coding Exercise

## API Documentation
* [Swagger Doc](http://localhost:8081/swagger-ui.html)

## Install Prerequisites

* [JDK 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
* Install using HomeBrew on mac and manage versions using jenv
```
brew update
brew tap homebrew/cask-versions
brew cask install java11
jenv add /Library/Java/JavaVirtualMachines/openjdk-11.0.2.jdk/Contents/Home
jenv local openjdk64-11.0.2

```

Alternatively, you can run it in docker. The only dependency is docker daemon.
* [Docker for Mac](https://docs.docker.com/docker-for-mac/install)
* [Docker for Windows](https://docs.docker.com/docker-for-windows/install)

## Build and Run program 
```
./run.sh
```

## Build & Run program in docker
```
./run-in-docker.sh
```

## Full integration test
```
cd integration-test
./run-test.sh

```

## CI tasks

Run style check, tests, coverage check and generate coverage report
```
./gradlew clean check test jacocoTestCoverageVerification jacocoTestReport
```

Alternatively, just run ci script 

```
./ci.sh
```

## Other Documents
* [Problem to solve](./FinCrime Coding Exercise.docx)
* [Assumptions](./ASSUMPTIONS.md) 