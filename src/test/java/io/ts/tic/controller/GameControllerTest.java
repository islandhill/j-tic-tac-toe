package io.ts.tic.controller;
import io.ts.tic.exception.GameNotFoundException;
import io.ts.tic.exception.GameStatusException;
import io.ts.tic.model.Game;
import io.ts.tic.service.GameService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.ArrayList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameService gameService;

    private String baseUrl = "/api/games";
    private String gameId = "123";

    @Test
    public void getAllGames() throws Exception {
        when(gameService.getGames()).thenReturn(new ArrayList<>());
        mockMvc.perform(MockMvcRequestBuilders.get(baseUrl).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getGame() throws Exception {
        Game game = new Game(gameId);
        when(gameService.getGame(gameId)).thenReturn(game);
        mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/" + gameId).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void gameNotFoundWhenGetGame() throws Exception {
        when(gameService.getGame(gameId)).thenThrow(new GameNotFoundException("Not found!"));
        mockMvc.perform(MockMvcRequestBuilders.get(baseUrl + "/" + gameId).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void addGame() throws Exception {
        Game game = new Game(gameId);
        when(gameService.addGame()).thenReturn(game);
        mockMvc.perform(MockMvcRequestBuilders.post(baseUrl + "/new").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void move() throws Exception {
        Integer position = 0;
        String url = baseUrl + "/" + gameId + "/move/" + position;
        Game gameAfterMove = new Game(gameId);
        gameAfterMove.getSquare()[0] = "X";
        when(gameService.move(gameId, position)).thenReturn(gameAfterMove);
        mockMvc.perform(MockMvcRequestBuilders.put(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void gameStatusProblemWhenMove() throws Exception {
        Integer position = 0;
        String url = baseUrl + "/" + gameId + "/move/" + position;
        when(gameService.move(gameId, position)).thenThrow(new GameStatusException("status wrong"));
        mockMvc.perform(MockMvcRequestBuilders.put(url).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void unexpectedException() throws Exception {
        when(gameService.getGames()).thenThrow(new RuntimeException());
        mockMvc.perform(MockMvcRequestBuilders.get(baseUrl).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());

    }

}
