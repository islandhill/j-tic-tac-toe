package io.ts.tic.service;

import io.ts.tic.exception.GameNotFoundException;
import io.ts.tic.exception.GameStatusException;
import io.ts.tic.model.Game;
import io.ts.tic.model.GameStatus;
import io.ts.tic.model.Turn;
import io.ts.tic.repository.GameRepository;
import org.javatuples.Triplet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private String gameId = "123";

    @InjectMocks
    private GameService gameService;

    @Mock
    private GameRepository gameRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getGames() {
        List<Game> games = new ArrayList<>() {{
            add(new Game());
        }};

        when(gameRepository.getGames()).thenReturn(games);
        Assert.assertEquals(games, gameService.getGames());
    }

    @Test(expected = GameNotFoundException.class)
    public void idCannotBeFoundWhenGetGame() {
        Game game = new Game(gameId);
        when(gameRepository.getGame(gameId)).thenReturn(null);
        gameService.getGame(gameId);
    }


    @Test
    public void getGame() {
        Game game = new Game(gameId);
        when(gameRepository.getGame(gameId)).thenReturn(game);
        Assert.assertEquals(gameId, gameService.getGame(gameId).getId());
    }

    @Test
    public void addGame() {
        Game game = new Game(gameId);
        when(gameRepository.addGame(any(Game.class))).thenReturn(game);
        Assert.assertEquals(gameId, gameService.addGame().getId());
    }

    @Test(expected = GameNotFoundException.class)
    public void idCannotBeFoundWhenMove() {
        when(gameRepository.getGame(gameId)).thenReturn(null);
        gameService.move(gameId, 0);
    }

    @Test(expected = GameStatusException.class)
    public void gameFinishedWhenMove() {
        Game game = new Game(gameId);
        game.setStatus(GameStatus.setWinner(Turn.O));
        when(gameRepository.getGame(gameId)).thenReturn(game);
        gameService.move(gameId, 8);
    }

    @Test(expected = GameStatusException.class)
    public void invalidPositionWhenMove() {
        Game game = new Game(gameId);
        when(gameRepository.getGame(gameId)).thenReturn(game);
        gameService.move(gameId, 9);
    }

    @Test(expected = GameStatusException.class)
    public void positionAlreadyOccupiedWhenMove() {
        Game game = new Game(gameId);
        game.setSquare(new String[] {"X", "X", "O", null, null, null, null, null, "O"});
        when(gameRepository.getGame(gameId)).thenReturn(game);
        gameService.move(gameId, 8);
    }

    @Test
    public void happyDaysWhenMove() {
        Game game = new Game(gameId);
        game.setSquare(new String[] {"X", "X", "O", null, null, null, null, null, "O"});
        when(gameRepository.getGame(gameId)).thenReturn(game);

        Game gameAfterMove = gameService.move(gameId, 7);
        String[] expectedSquare = new String[] {"X", "X", "O", null, null, null, null, "X", "O"};
        Assert.assertArrayEquals(expectedSquare, gameAfterMove.getSquare());
        Assert.assertEquals(Turn.O, gameAfterMove.getTurn());
        Assert.assertFalse(gameAfterMove.getStatus().isFinished());
    }

    @Test
    public void checkGameStatus() {

        List<Triplet<String, Game, GameStatus>> testData = new ArrayList<>();
        testData.add(
                Triplet.with("Game 1 winner is X",
                new Game().setSquare(new String[]{"X", "X", "X", "O", "O", "X", "O", "O", "X"}),
                GameStatus.setWinner(Turn.X)
        ));
        testData.add(
                Triplet.with("Game 2 winner is O",
                        new Game().setSquare(new String[]{"O", "X", "X", "X", "O", "X", "X", "O", "O"}),
                        GameStatus.setWinner(Turn.O)
                ));
        testData.add(
                Triplet.with("Game 3 is unfinished",
                        new Game().setSquare(new String[]{"X", "X", "O", null, null, null, null, null, "O"}),
                        new GameStatus()
                ));
        testData.add(
                Triplet.with("Game 4 is inconclusive",
                        new Game().setSquare(new String[]{"X", "X", "O", "O", "O", "X", "X", "O", "X"}),
                        GameStatus.setInconclusive()
                ));
        testData.forEach(each -> {
            logger.info("Testing checkGameStatus: " + each.getValue0());
            Assert.assertEquals(each.getValue2(), gameService.checkGameStatus(each.getValue1()));
        });
    }
}
