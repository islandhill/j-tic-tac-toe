package io.ts.tic.model;

import org.junit.Assert;
import org.junit.Test;
import java.util.List;

public class GameTest {
    private Game game;

    @Test
    public void isValidPosition() {
        game = new Game();
        game.getSquare()[0] = Turn.X.toString();
        game.getSquare()[1] = Turn.O.toString();
        Assert.assertTrue(game.isValidPosition(2));
        Assert.assertFalse(game.isValidPosition(9));
        Assert.assertFalse(game.isValidPosition(1));
        Assert.assertFalse(game.isValidPosition(null));
        Assert.assertFalse(game.isValidPosition(-1));
    }

    @Test
    public void switchTurn() {
        game = new Game();
        List<Turn> turnsInOrder = List.of(Turn.X, Turn.O, Turn.X, Turn.O);
        turnsInOrder.forEach(each -> {
            Assert.assertEquals(each, game.getTurn());
            game.switchTurn();
        });
    }
}
