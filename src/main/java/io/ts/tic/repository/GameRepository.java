package io.ts.tic.repository;

import io.ts.tic.model.Game;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Component
public class GameRepository {
    private Map<String, Game> games = new HashMap<>();

    public Game addGame(Game game) {
        String id = UUID.randomUUID().toString();
        game.setId(id);
        games.put(game.getId(), game);
        return games.get(id);
    }

    public Game getGame(String id) {
        return games.get(id);
    }

    public List<Game> getGames() {
        return new ArrayList<>(games.values());
    }
}
