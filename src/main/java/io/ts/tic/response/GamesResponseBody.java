package io.ts.tic.response;

import io.swagger.annotations.ApiModel;
import io.ts.tic.model.Game;
import java.util.List;

@ApiModel("Response body to return a list of games or error")
public class GamesResponseBody {

    private List<Game> data;
    private ErrorDetails errors;

    public GamesResponseBody(List<Game> data) {
        this.data = data;
    }

    public GamesResponseBody(ErrorDetails errors) {
        this.errors = errors;
    }

    public List<Game> getData() {
        return data;
    }

    public void setData(List<Game> data) {
        this.data = data;
    }

    public ErrorDetails getErrors() {
        return errors;
    }

    public void setErrors(ErrorDetails errors) {
        this.errors = errors;
    }
}




