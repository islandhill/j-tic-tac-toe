package io.ts.tic.service;

import io.ts.tic.exception.GameNotFoundException;
import io.ts.tic.exception.GameStatusException;
import io.ts.tic.model.Game;
import io.ts.tic.model.GameStatus;
import io.ts.tic.model.Turn;
import io.ts.tic.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class GameService {

    @Autowired
    private GameRepository gameRepository;

    private static final List<List<Integer>> WINNING_LINES = List.of(
            List.of(0, 1, 2),
            List.of(3, 4, 5),
            List.of(6, 7, 8),

            List.of(0, 4, 8),
            List.of(2, 4, 6),

            List.of(0, 3, 6),
            List.of(1, 4, 7),
            List.of(2, 5, 8)
    );

    public List<Game> getGames() {
        return gameRepository.getGames();
    }

    public Game getGame(String gameId) {
        Game game = gameRepository.getGame(gameId);
        if (game == null) {
            throw new GameNotFoundException("Game with id " + gameId + "cannot be found.");
        }
        return game;
    }

    public Game addGame() {
        Game game = new Game();
        return gameRepository.addGame(game);
    }

    public Game move(String gameId, Integer position) {
        Game game = gameRepository.getGame(gameId);
        if (game == null) {
            throw new GameNotFoundException("Game with id " + gameId + "cannot be found.");
        }

        if (game.getStatus().isFinished()) {
            throw new GameStatusException("Game with id " + gameId + "has finished.");
        }

        if (game.isValidPosition(position)) {
            game.getSquare()[position] = game.getTurn().toString();
            game.switchTurn();
            game.setStatus(this.checkGameStatus(game));
            return game;
        } else {
            throw new GameStatusException("Cannot move to the new position "
                    + position + ". It's either invalid or has already been occupied.");
        }
    }


    public GameStatus checkGameStatus(Game game) {
        String[] square = game.getSquare();

        for (List<Integer> line : WINNING_LINES) {
            Integer pos1 = line.get(0);
            Integer pos2 = line.get(1);
            Integer pos3 = line.get(2);
            if (square[pos1] != null && square[pos1] == square[pos2] && square[pos2] == square[pos3]) {
                //found winner
                return GameStatus.setWinner(Turn.valueOf(square[pos1]));
            }
        }

        if (Arrays.stream(square).noneMatch(Objects::isNull)) {
            //finished with a tie
            return GameStatus.setInconclusive();
        }

        //unfinished
        return new GameStatus();
    }
}
