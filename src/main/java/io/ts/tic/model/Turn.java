package io.ts.tic.model;

import io.swagger.annotations.ApiModel;
@ApiModel(description = "Enum value to model who's turn. It's either X or O")
public enum Turn {
    X,
    O
}
