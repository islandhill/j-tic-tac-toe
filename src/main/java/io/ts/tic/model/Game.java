package io.ts.tic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import static io.ts.tic.model.Turn.O;
import static io.ts.tic.model.Turn.X;


@ApiModel(description = "All details about the game")
public class Game {

    @ApiModelProperty(notes = "UUID for a game")
    private String id;

    @ApiModelProperty(notes = "square is an array of 9 with index from 0 to 8 that represents a game status",
            example = "[X, X, X, O, O, null, null, null, null]")
    private String[] square;

    @ApiModelProperty(notes = "indicates it's either X's or O's turn")
    private Enum turn;
    private GameStatus status;
    private static final Integer DEFAULT_SQUARE_SIZE = 9;

    public Game() {
        square = new String[DEFAULT_SQUARE_SIZE];
        turn = X;
        status = new GameStatus();
    }

    public Game(String id) {
        this();
        this.id = id;
    }

    public boolean isValidPosition(Integer position) {
        return position != null
                && position >= 0
                && position <= DEFAULT_SQUARE_SIZE - 1
                && square[position] == null;
    }

    public void switchTurn() {
        if (X.equals(this.turn)) {
            this.turn = O;
        } else {
            this.turn = X;
        }
    }

    public String getId() {
        return id;
    }

    public String[] getSquare() {
        return square;
    }

    //for test
    public Game setSquare(String[] square) {
        this.square = square;
        return this;
    }

    public Enum getTurn() {
        return turn;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }
}
