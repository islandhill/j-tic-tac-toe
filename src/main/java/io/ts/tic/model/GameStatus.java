package io.ts.tic.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(description = "All details about the game")
public class GameStatus {

    @ApiModelProperty(notes = "Boolean value to show if a game is finished or not ")
    private boolean isFinished = false;

    @ApiModelProperty(notes = "Boolean value to show if a game is conclusive or not. e.g. false means a game is a tie")
    private boolean isConclusive = true;

    @ApiModelProperty(notes = "Either X or O who is the winner when the game is finished")
    private Turn winner;

    public GameStatus() {
    }

    public static GameStatus setInconclusive() {
        GameStatus gameStatus = new GameStatus();
        gameStatus.isConclusive = false;
        gameStatus.isFinished = true;
        return gameStatus;
    }

    public static GameStatus setWinner(Turn winner) {
        GameStatus gameStatus = new GameStatus();
        gameStatus.winner = winner;
        gameStatus.isFinished = true;
        gameStatus.isConclusive = true;
        return gameStatus;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public boolean isConclusive() {
        return isConclusive;
    }

    public Turn getWinner() {
        return winner;
    }

    @Override
    public boolean equals(Object obj) {
        GameStatus gameStatus = (GameStatus) obj;
        return this.isFinished == gameStatus.isFinished
                && this.isConclusive == gameStatus.isConclusive
                && this.winner == gameStatus.winner;
    }
}
