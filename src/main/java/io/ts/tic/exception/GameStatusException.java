package io.ts.tic.exception;

public class GameStatusException extends RuntimeException {

    public GameStatusException(String message) {
        super(message);
    }
}
