package io.ts.tic.controller;

import io.ts.tic.exception.GameNotFoundException;
import io.ts.tic.exception.GameStatusException;
import io.ts.tic.response.ErrorDetails;
import io.ts.tic.response.GamesResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@SuppressWarnings("unchecked")
public class RestExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = {GameStatusException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ResponseBody> badRequest(GameStatusException e) {
        return getFailResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler(value = {GameNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ResponseBody> gameNotFound(GameNotFoundException e) {
        return getFailResponse(HttpStatus.NOT_FOUND, e);
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ResponseBody> unexpectedException(Exception e) {
        return getFailResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    private ResponseEntity<ResponseBody> getFailResponse(HttpStatus httpStatus, Exception e) {
        logger.error("Error occurred: ", e.getMessage(), e);
        return new ResponseEntity(new GamesResponseBody(new ErrorDetails(e.getMessage())), httpStatus);
    }
}
