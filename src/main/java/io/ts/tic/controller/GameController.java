package io.ts.tic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.ts.tic.model.Game;
import io.ts.tic.response.GamesResponseBody;
import io.ts.tic.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/api/games", produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
@CrossOrigin
@Api(value = "games API endpoints", description = "")
@SuppressWarnings("unchecked")
public class GameController {

    @Autowired
    private GameService gameService;

    @GetMapping
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Success", response = GamesResponseBody.class
            )
    })
    public ResponseEntity<ResponseBody> getAllGames() {
        return getSuccessResponse(HttpStatus.OK, gameService.getGames().toArray(new Game[0]));
    }

    @GetMapping(value = "/{gameId}")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Success", response = GamesResponseBody.class
            )
    })
    public ResponseEntity<ResponseBody> getGame(@PathVariable String gameId) {
        return getSuccessResponse(HttpStatus.OK, gameService.getGame(gameId));
    }

    @PostMapping(value = "/new")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Success", response = GamesResponseBody.class
            )
    })
    public ResponseEntity<ResponseBody> addGame() {
        return getSuccessResponse(HttpStatus.CREATED, gameService.addGame());
    }

    @PutMapping(value = "/{gameId}/move/{position}")
    @ApiResponses(value = {

            @ApiResponse(code = 200, message = "Success", response = GamesResponseBody.class
            )
    })
    public ResponseEntity<ResponseBody> move(@PathVariable String gameId, @PathVariable Integer position) {
        return getSuccessResponse(HttpStatus.OK, gameService.move(gameId, position));
    }

    private ResponseEntity<ResponseBody> getSuccessResponse(HttpStatus httpStatus, Game... games) {
        List<Game> gameList = Arrays.asList(games);
        return new ResponseEntity(new GamesResponseBody(gameList), httpStatus);
    }

}
