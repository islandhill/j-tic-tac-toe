#!/bin/bash

set -euxo pipefail

./gradlew clean check test jacocoTestCoverageVerification jacocoTestReport
