const api = require('./api');

describe('tic tac toe API testing', () => {

  test('Add two games then Get all games', async () => {
    const response1 = await api.addGame();
    const response2 = await api.addGame();
    const response3 = await api.getAllGames();
    const id1 = response1.data.data[0].id;
    const id2 = response2.data.data[0].id;
    const {data} = response3.data;
    expect(response3.data.data.filter(game => [id1, id2].includes(game.id)).length).toBe(2);
  });

  test('Add a new game then retrieve the game', async () => {
    const response = await api.addGame();
    expect(response.status).toBe(201);
    const {data} = response.data;
    const {id, square, turn, status} = data[0];
    expect(id).toBeDefined();
    expect(square.length).toBe(9);
    expect(square.filter(each => each !== null).length).toBe(0);
    expect(turn).toBe('X');
    expect(status).toMatchObject({"conclusive": true, "finished": false, "winner": null});

    const response2 = await api.getGame(id);
    expect(response2.status).toBe(200);
  });

  test('Add a new game then play the game to get a winner', async () => {
    const response1 = await api.addGame();
    const gameId1 = response1.data.data[0].id;
    console.log("Start making a few moves and finishing the game");
    console.log("attempting to get [XXXOO]")
    await api.move(gameId1, 0);
    await api.move(gameId1, 3);
    await api.move(gameId1, 1);
    await api.move(gameId1, 4);
    await api.move(gameId1, 2);

    const response2 = await api.getGame(gameId1);
    const {id, square, turn, status} = response2.data.data[0];
    expect(id).toBeDefined();
    expect(square).toMatchObject(['X', 'X', 'X', 'O', 'O', null, null, null, null]);
    expect(status).toMatchObject({"conclusive": true, "finished": true, "winner": 'X'});
  });

});