const axios = require('axios');
const api = axios.create({
  baseURL: process.env.API_BASEURL || 'http://localhost:8081'
});

module.exports = {
  getAllGames: async function () {
    return await api.request({
      method: 'GET',
      url: '/api/games'
    });
  },
  addGame: async function () {
    return await api.request({
      method: 'POST',
      url: '/api/games/new'
    });
  },
  getGame: async function (id) {
    return await api.request({
      method: 'GET',
      url: `/api/games/${id}`
    });
  },
  move: async function (id, position) {
    return await api.request({
      method: 'PUT',
      url: `/api/games/${id}/move/${position}`
    });
  }
};