#!/bin/bash

set -euxo pipefail

PROGRAM_NAME="app-1.0.0.jar"

./gradlew clean build
java -jar ./build/libs/${PROGRAM_NAME}
