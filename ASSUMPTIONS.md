## Here is a list of assumptions made for this exercise
* Thread safety is not considered so HashMap is used for storing the games in memory 
* When a new game starts, "X" always has the turn first
* seeking for a generic algorithm to determine the winning or inclusive conditions is out of scope